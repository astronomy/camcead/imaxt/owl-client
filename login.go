// login.go

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/howeyc/gopass"
)

func login() {
	// Prompt user for username and password
	fmt.Print("Username: ")
	var username string
	fmt.Scanln(&username)

	fmt.Print("Password: ")
	password, err := gopass.GetPasswdMasked()
	if err != nil {
        fmt.Println("Error:", err)
		os.Exit(1)
    }

	// Send login request to REST API
	url := "https://darkroom.ast.cam.ac.uk/api/v1/accounts/token/"
	payload := fmt.Sprintf(`{"username": "%s", "password": "%s"}`, username, password)
	req, err := http.NewRequest("POST", url, strings.NewReader(payload))
	if err != nil {
		fmt.Println("Error creating request:", err)
		os.Exit(1)
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	// print response
	// body, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	fmt.Println("Error reading response:", err)
	// 	return err
	// }
	// fmt.Println(string(body))
	

	// Parse login response
	var loginResponse LoginResponse
	err = json.NewDecoder(resp.Body).Decode(&loginResponse)
	if err != nil {
		fmt.Println("Error decoding response:", err)
		os.Exit(1)
	}

	if loginResponse.AccessToken == "" {
		fmt.Println("Error: Invalid username or password")
		os.Exit(1)
	}

	// Save token to file
	err = saveToken(loginResponse.AccessToken)
	if err != nil {
		fmt.Println("Error saving token:", err)
		os.Exit(1)
	}

	// Print token
	fmt.Println("Login successful!")
}
