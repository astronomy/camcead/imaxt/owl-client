module github.com/eddienko/owl

go 1.18

require github.com/howeyc/gopass v0.0.0-20210920133722-c8aef6fb66ef

require (
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/term v0.7.0 // indirect
)
