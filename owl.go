package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"strings"
)

type LoginResponse struct {
	RefreshToken string `json:"refresh"`
	AccessToken  string `json:"access"`
	User         struct {
		Username  string `json:"username"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Email     string `json:"email"`
		IsActive  bool   `json:"is_active"`
		IsStaff   bool   `json:"is_staff"`
		IsSuperuser bool  `json:"is_superuser"`
	} `json:"user"`
}

type UserInfo struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

func main() {
	

	// Define the debug flag
	debug := flag.Bool("debug", false, "Print verbose output")

	// Parse the command line arguments
	flag.Parse()

	if len(flag.Args()) < 1 {
		fmt.Println("Usage: owl [--debug] <command>")
		fmt.Println("Commands:")
		fmt.Println("  login   Login to the system")
		fmt.Println("  whoami  Print information about the current user")
		os.Exit(1)
	}

	// Print debug info
	if *debug {
		fmt.Println("Debug mode enabled")
	}

	// apiURL := os.Getenv("DARKROOM_URL")
	// if apiURL == "" {
	// 	fmt.Println("DARKROOM_URL environment variable is not set")
	// 	// Handle the error here
	// } else {
	// 	fmt.Printf("API URL: %s\n", apiURL)
	// 	// Use the API URL here
	// }

	switch flag.Arg(0) {
	case "login":
		login()
	case "submit":
		// Handle submit command
		if len(flag.Args()) != 2 {
			fmt.Println("Usage: owl submit <file>")
			os.Exit(1)
		}
		filename := flag.Arg(1)
		fmt.Println("Submitting file:", filename)
		submit(filename)
	case "whoami":
		whoami()
	default:
		fmt.Println("Unknown command:", flag.Arg(0))
		os.Exit(1)
	}

	
}

// write the submit function
func submit(filename string) {
	// Load token from file
	token, err := loadToken()
	if err != nil {
		fmt.Println("Error loading token:", err)
		os.Exit(1)
	}

	// Read the contents of the file
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error reading file:", err)
		os.Exit(1)
	}

	// Send submit request to REST API
	url := "http://example.com/api/submit"
	payload := fmt.Sprintf(`{"filename": "%s", "content": "%s"}`, filename, content)
	req, err := http.NewRequest("POST", url, strings.NewReader(payload))
	if err != nil {
		fmt.Println("Error creating request:", err)
		os.Exit(1)
	}

	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	// Print the HTTP response
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		os.Exit(1)
	}
	fmt.Println("HTTP response:", string(respBody))

}

func whoami() {
	// Load token from file
	token, err := loadToken()
	if err != nil {
		fmt.Println("Error loading token:", err)
		os.Exit(1)
	}

	// Send whoami request to REST API
	url := "http://example.com/api/whoami"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		os.Exit(1)
	}

	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	// Parse user info response
	var userInfo UserInfo
	err = json.NewDecoder(resp.Body).Decode(&userInfo)
	if err != nil {
		fmt.Println("Error decoding response:", err)
		os.Exit(1)
	}

	// Print user info
	fmt.Println("User ID:", userInfo.ID)
	fmt.Println("Username:", userInfo.Username)
	fmt.Println("Email:", userInfo.Email)
}

func loadToken() (string, error) {
	// Get user's home directory
	usr, err := user.Current()
	if err != nil {
		return "", err
	}

	// Load token from file
	rcFilePath := fmt.Sprintf("%s/.owlrc", usr.HomeDir)
	rcFile, err := os.Open(rcFilePath)
	if err != nil {
		return "", err
	}
	defer rcFile.Close()

	rcBytes, err := ioutil.ReadAll(rcFile)
	if err != nil {
		return "", err
	}

	token := string(rcBytes)
	return strings.TrimSpace(token), nil
}

func saveToken(token string) error {
	// Get user's home directory
	usr, err := user.Current()
	if err != nil {
		return err
	}

	// Create .owlrc file if it doesn't exist
	rcFilePath := fmt.Sprintf("%s/.owlrc", usr.HomeDir)
	rcFile, err := os.OpenFile(rcFilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	defer rcFile.Close()

	// Write token to file
	_, err = rcFile.WriteString(token)
	if err != nil {
		return err
	}

	return nil
}